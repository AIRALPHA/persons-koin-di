package com.airalpha.koinperson.di

import com.airalpha.koinperson.viewmodels.PersonViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {

    //fun provideViewModel(repository: PersonRepository) {
    //    val factory = PersonViewModelFactory(repository)
    //    return ViewModelProvider(this, factory)[PersonViewModel::class.java]
    //}

    viewModel {
        PersonViewModel(repository = get())
    }
}