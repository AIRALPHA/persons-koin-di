package com.airalpha.koinperson.di

import android.app.Application
import androidx.room.Room
import com.airalpha.koinperson.data.dao.PersonDao
import com.airalpha.koinperson.data.db.PersonDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module


val databaseModule = module {

    fun provideDatabase(application: Application): PersonDatabase {
        return Room.databaseBuilder(
            application,
            PersonDatabase::class.java,
            "person_database.db"
        ).fallbackToDestructiveMigration()
            .build()
    }

    fun providePersonDao(database: PersonDatabase): PersonDao {
        return database.getPersonDao()
    }

    single { provideDatabase(androidApplication()) }
    single { providePersonDao(get()) }
}