package com.airalpha.koinperson.di

import com.airalpha.koinperson.domain.PersonRepository
import com.airalpha.koinperson.data.PersonRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    factory<PersonRepository> { PersonRepositoryImpl(get()) }
}