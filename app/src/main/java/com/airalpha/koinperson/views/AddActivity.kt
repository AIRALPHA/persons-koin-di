package com.airalpha.koinperson.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import com.airalpha.koinperson.data.models.Person
import com.airalpha.koinperson.viewmodels.PersonViewModel
import com.airalpha.koinperson.R
import kotlinx.android.synthetic.main.activity_add.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddActivity : AppCompatActivity() {

    private val viewModel by viewModel<PersonViewModel>()

    private var person: Person? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

        person = intent.extras?.getParcelable("NOTE_DATA")

        if(person != null) {
            firstname.text = Editable.Factory.getInstance().newEditable(person?.firstname ?: "")
            lastname.text = Editable.Factory.getInstance().newEditable(person?.lastname ?: "")

            addButton.text = "Modifier"
        }

        addButton.setOnClickListener {
            addPerson()
        }
    }

    private fun addPerson() {
        val id = if(person != null) person?.id else null
        val firstName = firstname.text.toString()
        val lastName = lastname.text.toString()

        val person = Person(id, firstName, lastName)

        CoroutineScope(Dispatchers.Main).launch {
            if(id != null) {
                viewModel.updatePerson(person).also {
                    //Toast.makeText(this, "Person ajouté", Toast.LENGTH_LONG).show()
                    finish()
                }
            } else {
                viewModel.insertPerson(person).also {
                    //Toast.makeText(this, "Person ajouté", Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}