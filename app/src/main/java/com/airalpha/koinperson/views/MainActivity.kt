package com.airalpha.koinperson.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.airalpha.koinperson.data.models.Person
import com.airalpha.koinperson.viewmodels.PersonViewModel
import com.airalpha.koinperson.R
import com.airalpha.koinperson.domain.InjectActivity
import com.airalpha.koinperson.utils.UtilExtensions.openActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var personAdapter: PersonAdapter
    lateinit var viewModel: PersonViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as InjectActivity).injectActivity(this)
        // Adapter and recyclerview
        personAdapter = PersonAdapter(
            listener = {person: Person -> openActivity(AddActivity::class.java){
                putParcelable("NOTE_DATA", person)
            } },
            onclick = {person: Person -> deletePerson(person) }
        )
        person_recyclerview.layoutManager = LinearLayoutManager(this)
        person_recyclerview.adapter = personAdapter

        addButtonFAB.setOnClickListener {
            openActivity(AddActivity::class.java)
        }

        CoroutineScope(Dispatchers.Main).launch {
            viewModel.getAllPersons().observe(this@MainActivity, {
                personAdapter.submitList(it)
            })
        }
    }

    fun deletePerson(person: Person) {
        CoroutineScope(Dispatchers.Main).launch {
            viewModel.deletePerson(person)
        }
    }
}