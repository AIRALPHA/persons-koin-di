package com.airalpha.koinperson.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.airalpha.koinperson.data.models.Person
import com.airalpha.koinperson.R
import kotlinx.android.synthetic.main.item_person.view.*

class PersonAdapter(private val listener: (Person) -> Unit, private val onclick: (Person) -> Unit): ListAdapter<Person, PersonAdapter.PersonViewHolder>(
    DiffUtilPerson()
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PersonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_person, parent, false)
        return PersonViewHolder(view)
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val item = getItem(position)
        holder.bindItem(item, listener, onclick)
    }


    class PersonViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bindItem(person: Person, listener: (Person) -> Unit, onclick: (Person) -> Unit) {
            view.apply {
                firstname_tv.text = person.firstname
                lastname_tv.text = person.lastname
                deleteImage.setOnClickListener {
                    print("Click")
                    onclick(person)
                }
            }

            view.setOnClickListener {
                print("Click 2")
                listener(person)
            }
        }
    }

    private class DiffUtilPerson: DiffUtil.ItemCallback<Person>() {
        override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean {
            return newItem.id == oldItem.id
        }

        override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean {
            return newItem == oldItem
        }
    }
}