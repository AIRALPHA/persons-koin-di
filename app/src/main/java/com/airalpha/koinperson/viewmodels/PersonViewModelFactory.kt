package com.airalpha.koinperson.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.airalpha.koinperson.domain.PersonRepository
import java.lang.Exception

class PersonViewModelFactory(private val repository: PersonRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        try {
            val constructor = modelClass.getDeclaredConstructor(PersonRepository::class.java)
            return constructor.newInstance(repository)
        } catch (e: Exception) {
            Log.e("ERROR", "ERROR -> ${e.message}")
        }

        return super.create(modelClass)
    }
}