package com.airalpha.koinperson.viewmodels

import androidx.lifecycle.ViewModel
import com.airalpha.koinperson.data.models.Person
import com.airalpha.koinperson.domain.PersonRepository

class PersonViewModel(private val repository: PersonRepository): ViewModel() {

    suspend fun insertPerson(person: Person) = repository.insertPerson(person)

    suspend fun updatePerson(person: Person) = repository.updatePerson(person)

    suspend fun deletePerson(person: Person) = repository.deletePerson(person)

    suspend fun deletePersonById(id: Int) = repository.deletePersonById(id)

    fun getAllPersons() = repository.getAllPersons()
}