package com.airalpha.koinperson

import android.app.Activity
import android.app.Application
import com.airalpha.koinperson.di.databaseModule
import com.airalpha.koinperson.di.repositoryModule
import com.airalpha.koinperson.di.viewModelModule
import com.airalpha.koinperson.domain.InjectActivity
import com.airalpha.koinperson.viewmodels.PersonViewModel
import com.airalpha.koinperson.views.AddActivity
import com.airalpha.koinperson.views.MainActivity
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

internal class KoinPersonApplication: Application(), InjectActivity {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@KoinPersonApplication)
            modules(
                viewModelModule,
                repositoryModule,
                databaseModule
            )
        }
    }

    override fun injectActivity(activity: Activity) {
        when(activity) {
            is MainActivity -> {
                activity.viewModel = inject<PersonViewModel>().value
            }
        }
    }
}