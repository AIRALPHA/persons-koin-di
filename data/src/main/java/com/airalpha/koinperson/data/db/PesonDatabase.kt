package com.airalpha.koinperson.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.airalpha.koinperson.data.models.Person
import com.airalpha.koinperson.data.dao.PersonDao

@Database(
    entities = [Person::class],
    version = 1,
    exportSchema = false
)

abstract class PersonDatabase: RoomDatabase() {
    abstract fun getPersonDao(): PersonDao
}