package com.airalpha.koinperson.data

import androidx.lifecycle.LiveData
import com.airalpha.koinperson.data.db.PersonDatabase
import com.airalpha.koinperson.data.models.Person
import com.airalpha.koinperson.domain.PersonRepository

class PersonRepositoryImpl(private val personDatabase: PersonDatabase): PersonRepository {
    override suspend fun insertPerson(person: Person) = personDatabase.getPersonDao().insertPerson(person)

    override suspend fun updatePerson(person: Person) = personDatabase.getPersonDao().updatePerson(person)

    override suspend fun deletePerson(person: Person) = personDatabase.getPersonDao().deletePerson(person)

    override suspend fun deletePersonById(id: Int) = personDatabase.getPersonDao().deletePersonById(id)

    override fun getAllPersons(): LiveData<List<Person>> = personDatabase.getPersonDao().getAllPersons()
}