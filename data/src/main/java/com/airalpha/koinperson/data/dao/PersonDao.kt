package com.airalpha.koinperson.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.airalpha.koinperson.data.models.Person

@Dao
interface PersonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPerson(person: Person)

    @Update
    suspend fun updatePerson(person: Person)

    @Delete
    suspend fun deletePerson(person: Person)

    @Query("SELECT * FROM person_table")
    fun getAllPersons(): LiveData<List<Person>>

    @Query("DELETE FROM person_table WHERE id = :id")
    suspend fun deletePersonById(id: Int)
}