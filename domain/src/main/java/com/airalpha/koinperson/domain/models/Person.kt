package com.airalpha.koinperson.data.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "person_table")
@Parcelize
data class Person(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    val firstname: String?,
    val lastname: String?
): Parcelable