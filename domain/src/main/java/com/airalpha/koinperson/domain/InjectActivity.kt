package com.airalpha.koinperson.domain

import android.app.Activity

interface InjectActivity {
    fun injectActivity(activity: Activity)
}