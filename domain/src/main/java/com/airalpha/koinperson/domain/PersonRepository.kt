package com.airalpha.koinperson.domain

import androidx.lifecycle.LiveData
import com.airalpha.koinperson.data.models.Person

interface PersonRepository {
    suspend fun insertPerson(person: Person)

    suspend fun updatePerson(person: Person)

    suspend fun deletePerson(person: Person)

    suspend fun deletePersonById(id: Int)

    fun getAllPersons(): LiveData<List<Person>>
}